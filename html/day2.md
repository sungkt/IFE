### HTML基础知识

1.标题 \<h1>---\<h6>

2.段落 \<p>

3.链接 \<a>

4.图像 \<img>

5.水平线 \<hr/>

6.换行 \<br/>

常用的一些语义化标签

 \<h1> ~ \<h6> ，作为标题使用，并且依据重要性递减，\<h1> 是最高的等级。

\<p>段落标记，知道了 \<p> 作为段落，你就不会再使用 \<br /> 来换行了，而且不需要 \<br /> 来区分段落与段落。\<p> 中的文字会自动换行，而且换行的效果优于 \<br />。段落与段落之间的空隙也可以利用 CSS 

\<ul>、\<ol>、\<li>，\<ul> 无序列表，这个被大家广泛的使用，\<ol> 有序列表不常用。在 Web 标准化过程中，\<ul> 还被更多的用于导航条，本来导航条就是个列表，这样做是完全正确的，而且当你的浏览器不支持 CSS 的时候，导航链接仍然很好使，只是美观方面差了一点而已。

\<dl>、\<dt>、\<dd>，\<dl> 就是“定义列表”。比如说词典里面的词的解释、定义就可以用这种列表。dl不单独使用，它通常与dt和dd一起使用。dl开启一个定义列表，dt表示要定义的项目名称，dd表示对dt的项目的描述。

\<em>、\<strong>，\<em> 是用作强调，\<strong> 是用作重点强调。

\<table>、\<thead>、\<tbody>、\<td>、\<th>、\<caption>， 就是用来做表格不要用来布局

### 新语义标签的兼容性处理

IE8 及以下版本的浏览器不支持 H5 和 CSS3。解决办法：引入`html5shiv.js`文件。

引入时，需要做if判断，具体代码如下：

```html
    <!--  条件注释 只有ie能够识别-->

    <!--[if lte ie 8]>
        <script src="html5shiv.min.js"></script>
    <![endif]-->
```
上方代码是**条件注释**：虽然是注释，但是IE浏览器可以识别出来。


## DOM 操作

### 获取元素

- document.querySelector("selector") 通过CSS选择器获取符合条件的第一个元素。

- document.querySelectorAll("selector")  通过CSS选择器获取符合条件的所有元素，以类数组形式存在。

### 类名操作

- Node.classList.add("class") 添加class

- Node.classList.remove("class") 移除class

- Node.classList.toggle("class") 切换class，有则移除，无则添加

- Node.classList.contains("class") 检测是否存在class

### 自定义属性

js 里可以通过 `box1.index=100;`  `box1.title` 来自定义属性和获取属性。

H5可以直接在标签里添加自定义属性，**但必须以 `data-` 开头**。


## 待深入学习

#### dom
#### 事件监听  拖拽 点击等